import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './view/home/home.component'
import { LoginComponent } from './view/login/login.component'
import { RegisterComponent } from './view/register/register.component'
import { TicketComponent } from './view/ticket/ticket.component';
import { AuthGuardService } from './_guards/auth-guard.service';


export const appRoutes: Routes = [

  { path: 'home', component: HomeComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuardService] },
  { path: 'register', component: RegisterComponent, canActivate: [AuthGuardService]},
  { path: 'ticket', component: TicketComponent, canActivate: [AuthGuardService]},
  
  // otherwise redirect to home
  { path: '', redirectTo: 'home', pathMatch: 'full', canActivate: [AuthGuardService]},
];


export const routing = RouterModule.forRoot(appRoutes);