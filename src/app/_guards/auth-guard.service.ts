import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { TokenStorageService } from '../_auth/token-storage.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private _router: Router,
    private tokenService: TokenStorageService) { }


  /**
   * Surcharge de la méthode CanActivate
   * 
   * @param next 
   * @param state 
   * @returns 
   */
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) :
  Observable<boolean> | Promise<boolean> | boolean {
    if (this.tokenService.getJWTToken()) {
        return true;  
    }

    // not logged in so redirect to login page with the return url
    this._router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    return false;
}

}