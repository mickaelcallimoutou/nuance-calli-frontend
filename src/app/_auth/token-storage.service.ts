import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { Tokens } from './token'

const TOKEN_KEY = 'AuthToken'
const REFRESH_TOKEN = 'REFRESH_TOKEN'
const USERNAME_KEY = 'AuthUsername'
const USERID_KEY = 'AuthUserId'
const AUTHORITIES_KEY = 'AuthAuthorities'
const PREFERENCS_KEY = 'AuthPreferences'
const NEWUSER_KEY = 'AuthIsNewUser'
@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  private roles: Array<string> = []

  constructor(private router: Router) { }

  /**
   * Déconnexion : Signout
   * Redirige l'utisateur sur la page de connexion
   */
  public signOut() {
    window.sessionStorage.clear();
    this.router.navigate(['/login'])
  }

  /**
   * Sauvagarde le token JWT dans la session de l'utilisateur
   * @param token JWT token (JSON)
   */
  public storeJWTToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY)
    window.sessionStorage.setItem(TOKEN_KEY, token)
  }

  /**
  * Sauvagarde le nouveau token JWT (refresh) dans la session de l'utilisateur
  * @param token JWT token (JSON)
  */
  public storeRefreshToken(refreshToken: string) {
    window.sessionStorage.removeItem(REFRESH_TOKEN)
    window.sessionStorage.setItem(REFRESH_TOKEN, refreshToken)
  }


  /**
   * Sauvegarde deux Token dans la session de l'utilisateur
   * @param tokens (JSON) : {jwt,refreshToken}
   */
  public storeTokens(tokens: Tokens) {
    window.sessionStorage.removeItem(TOKEN_KEY)
    window.sessionStorage.removeItem(REFRESH_TOKEN)
    window.sessionStorage.setItem(TOKEN_KEY, tokens.jwt)
    window.sessionStorage.setItem(REFRESH_TOKEN, tokens.refreshToken)
  }

  /**
   * @return token : jwt
   */
  public getJWTToken(): string | null {
    return window.sessionStorage.getItem(TOKEN_KEY)
  }

  /**
   * @return token : refreshToken
   */
  public getRefreshToken() {
    return window.sessionStorage.getItem(REFRESH_TOKEN)
  }


  // - - - - - - - - - - - USER ID - - - - - - - - - - -


  /**
   * Sauvegarde l'ID de l'utilisateur dans la session
   * @param userId 
   */
  public storeUserId(userId: number) {
    window.sessionStorage.removeItem(USERID_KEY)
    window.sessionStorage.setItem(USERID_KEY, userId.toString())
  }

  /**
   * @return username
   */
  public getUserId(): number {
    var userid_key = sessionStorage.getItem(USERID_KEY)
    return userid_key!=null?+userid_key:0
  }


  // - - - - - - - - - - - USERNAME - - - - - - - - - - -


  /**
   * Sauvegarde le login (username) de l'utilisateur dans la session
   * @param username 
   */
  public storeUsername(username: string) {
    window.sessionStorage.removeItem(USERNAME_KEY)
    window.sessionStorage.setItem(USERNAME_KEY, username)
  }

  /**
   * @return username
   */
  public getUsername(): string|null {
    return window.sessionStorage.getItem(USERNAME_KEY)
  }

  
  // - - - - - - - - - - - AUTHORITIES - - - - - - - - - - -

  /**
   * Sauvegarde les elements (Droits et Profiles) dans la session de l'utilisateur
   * @param authorities 
   */
  public storeAuthorities(authorities: string[]) {
    window.sessionStorage.removeItem(AUTHORITIES_KEY);
    window.sessionStorage.setItem(AUTHORITIES_KEY, JSON.stringify(authorities));
  }

  /**
   * @returns string[] : DDroits et Profiles)
   */
  public getAuthorities(): string[] {
    this.roles = []
    let authorities = sessionStorage.getItem(AUTHORITIES_KEY)
    
    if (sessionStorage.getItem(TOKEN_KEY) && authorities) {
      JSON.parse(authorities).forEach((authority: { authority: string }) => {
        this.roles.push(authority.authority)
      })
    }

    return this.roles
  }

  
  /**
   * 
   * @returns vrai si l'utilisateur possède le role : ADMIN
   */
  public isAdmin(): boolean {
    let isAdmin = false
    this.roles.forEach(role => {
      isAdmin = isAdmin || role=='ADMIN'
    })
    return isAdmin
  }

  // - - - - - - - - - - - NEW USER - - - - - - - - - - -

  /**
   * Sauvegarde le flag "NEWUSER" (nouvel utilisateur) dans la session de l'utilisateur
   * @param newUser 
   */
  public storeIsNewUser(newUser: boolean) {
    window.sessionStorage.removeItem(NEWUSER_KEY);
    window.sessionStorage.setItem(NEWUSER_KEY, newUser?'1':'0')
  }

  /**
   * @return vrai s'il s'agit de la première connexion de l'utilisateur
   */
  public isNewUser(): boolean {
    return sessionStorage.getItem(NEWUSER_KEY)=='1'?true:false
  }


}
