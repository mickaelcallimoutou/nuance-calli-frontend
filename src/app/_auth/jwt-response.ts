export class JwtResponse {
    accessToken: string
    refreshToken: string
    userId: number
    type: string
    newUser: boolean
    username: string
    authorities: string[]

    constructor(accessToken: string,
        refreshToken: string,
        userId: number,
        type: string,
        newUser: boolean,
        username: string,
        authorities: string[]) {
        this.accessToken = accessToken;
        this.username = username;
        this.refreshToken = refreshToken;
        this.userId = userId;
        this.type = type;
        this.newUser = newUser;
        this.authorities = authorities;
    }
}
