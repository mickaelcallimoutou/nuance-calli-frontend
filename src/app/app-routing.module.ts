import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './view/home/home.component';
import { LoginComponent } from './view/login/login.component';
import { RegisterComponent } from './view/register/register.component';
import { CreateTicketComponent } from './view/ticket/create-ticket/create-ticket.component';
import { SearchTicketComponent } from './view/ticket/search-ticket/search-ticket.component';
import { TicketComponent } from './view/ticket/ticket.component';
import { AuthGuardService } from './_guards/auth-guard.service';

export const appRoutes: Routes = [

  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'ticket', component: TicketComponent, canActivate: [AuthGuardService]},
  { path: 'ticket/create', component: CreateTicketComponent, canActivate: [AuthGuardService]},
  { path: 'ticket/search', component: SearchTicketComponent, canActivate: [AuthGuardService]},
  
  // otherwise redirect to home
  { path: '', redirectTo: 'home', pathMatch: 'full'},
];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
