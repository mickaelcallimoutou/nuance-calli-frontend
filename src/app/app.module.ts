import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegisterComponent } from './view/register/register.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './view/login/login.component';
import { HomeComponent } from './view/home/home.component';
import { TicketComponent } from './view/ticket/ticket.component';
import { HttpClientModule } from '@angular/common/http';
import { httpInterceptorProviders } from './_auth/auth-interceptor-provider';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CreateTicketComponent } from './view/ticket/create-ticket/create-ticket.component';
import { SearchTicketComponent } from './view/ticket/search-ticket/search-ticket.component';

import { MatCardModule} from '@angular/material/card';
import { MatIconModule} from '@angular/material/icon';

import { MatExpansionModule} from '@angular/material/expansion';
import { MatPaginatorModule} from '@angular/material/paginator';
import { MatTableModule} from '@angular/material/table';
import { MatDividerModule} from '@angular/material/divider';
import { MatTabsModule} from '@angular/material/tabs';
import { MatDialogModule} from '@angular/material/dialog';
import { MatStepperModule} from '@angular/material/stepper';
import { MatToolbarModule} from '@angular/material/toolbar';
import { MatRadioModule} from '@angular/material/radio';
import { MatFormFieldModule} from '@angular/material/form-field';



@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    TicketComponent,
    CreateTicketComponent,
    SearchTicketComponent,
  ],
  imports: [
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    FontAwesomeModule,
    MatCardModule,
    MatExpansionModule,
    MatIconModule,
    MatPaginatorModule,
    MatTableModule,
    MatDividerModule,
    MatTabsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatStepperModule,
    MatToolbarModule,
    MatRadioModule
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
