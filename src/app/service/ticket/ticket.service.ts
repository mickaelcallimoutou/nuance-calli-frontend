import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { api } from 'src/app/app.endpoint-back.api';
import { Ticket } from 'src/app/model/ticket';

@Injectable({
  providedIn: 'root'
})
export class TicketService {

  private uri_plural: string;
  private uri_single: string;
  private plural: string = "tickets";
  private single: string = "ticket";


  constructor(private _http: HttpClient) {
    this.uri_plural = `${api.endpoint_api_url}` + this.plural;
    this.uri_single = `${api.endpoint_api_url}` + this.single + "/";
  }


  /**
   * 
   * @returns 
   */
  search() : Observable<Ticket[]> {
    return this._http.get<Ticket[]>(this.uri_plural);
  }
  
  /**
   * 
   */
   getNews(): Observable<any[]> {
    return this._http.get<any[]>(this.uri_plural + '/news');
  }

  
  getAll(): Observable<Ticket[]> {
    return this._http.get<Ticket[]>(this.uri_plural);
  }

  getById(id: number): Observable<Ticket> {
    return this._http.get<Ticket>(this.uri_single + id);
  }

  create(ticket: Ticket): Observable<Ticket> {
    return this._http.post<Ticket>(this.uri_plural, ticket)
  }


  

} 
