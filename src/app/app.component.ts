import { Component } from '@angular/core';
import { faSignOutAlt, faUserAlt, faUserAstronaut } from '@fortawesome/free-solid-svg-icons';
import { TokenStorageService } from './_auth/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // FONT AWESOME
  faUserAstronaut = faUserAstronaut
  faSignOutAlt = faSignOutAlt
  faUser = faUserAlt

  title = 'nuance-calli-frontend'
  private roles: string[] = []
  username : any = ""
  showAdminBoard = false
  showModeratorBoard = false
  isAuthentified: boolean = false

  constructor(private tokenStorageService: TokenStorageService) { }

  ngOnInit(): void {
    this.isAuthentified = this.tokenStorageService.getJWTToken()?true:false

    if (this.isAuthentified) {
      this.username = this.tokenStorageService.getUsername()
      this.roles = this.tokenStorageService.getAuthorities()

    }
  }

  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }
}
