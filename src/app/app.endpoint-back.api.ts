export const api = {
  endpoint_api_url: 'http://localhost:3000/',

  endpoint_auth : 'auth',
  endpoint_login : 'login',
  endpoint_register : 'register'
};