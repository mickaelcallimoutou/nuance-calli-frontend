import { Component, OnInit } from '@angular/core';
import { Ticket } from 'src/app/model/ticket';
import { TicketService } from 'src/app/service/ticket/ticket.service';
import { FormBuilder, FormGroup } from "@angular/forms";
import { faPlus, faSearch } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'create-ticket',
  templateUrl: './create-ticket.component.html',
  styleUrls: ['./create-ticket.component.css']
})
export class CreateTicketComponent implements OnInit {

  form: FormGroup;

  ticket: Ticket = new Ticket();

  constructor(
    private _ticketService: TicketService,
    public formBuilder: FormBuilder
  ) { 
    this.form = this.formBuilder.group({
      title: [''],
      content: [null]
    })
  }

  ngOnInit() {
  }

  
  uploadFile(event: any) {
    if(event!=null && event.target) {
      // const file = (event.target as HTMLInputElement).files[0];
    }
    
    this.form.patchValue({
      // pj: file
    });
    // this.form.get('pj').updateValueAndValidity()
  }

  onItemSelect(item: any) {
  }

  onSelectAll(items: any) {
  }


  /**
   * 
   * @param Product 
   */
  create(ticket: Ticket) {
    this._ticketService.create(ticket)
      .subscribe(
        (reponse) => {
          this.clean()
        }
      );
  }


  /**
   * clean all the field
   */
  private clean(): void {
    this.ticket.title = ""
    this.ticket.content = ""
  }

}
