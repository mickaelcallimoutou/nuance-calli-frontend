import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Ticket } from 'src/app/model/ticket';
import { TicketService } from '../../../service/ticket/ticket.service';

@Component({
  selector: 'app-search-ticket',
  templateUrl: './search-ticket.component.html',
  styleUrls: ['./search-ticket.component.css']
})
export class SearchTicketComponent implements OnInit {
 
    
  
    // pager object
    pager: any = {}
    tickets: Ticket[] = []
    ticket: Ticket = {}
  
    tickets_source :  MatTableDataSource<Ticket>
    editIndex = 0;
    @ViewChild(MatPaginator, {static:false}) paginator?: MatPaginator
    
    displayedColumns: string[] = ['id','name',  'domainCode', 'actions']
  
    // paged items
    pagedItems: any[]
  
  
    constructor(
      private _ticketService: TicketService,
    ) {
      this.pagedItems = []
      this.tickets_source = new MatTableDataSource
    }
  
    ngOnInit() {
      this.search_product()
    }
  
  
    search_product() {
      this._ticketService.search()
        .subscribe((reponse) => {
          this.tickets = reponse 
          this.tickets_source = new MatTableDataSource(this.tickets);
        })
    }
  
  
  }
  
